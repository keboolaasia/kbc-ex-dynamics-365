FROM frolvlad/alpine-python3:latest

RUN mkdir -p /data/out/tables /data/in/tables /data/out/files /data/in/files
RUN apk add --no-cache git
RUN pip install --no-cache-dir --ignore-installed  \
  requests \
  pytest \
  https://github.com/keboola/python-docker-application/archive/2.0.0.zip \
  git+https://bitbucket.org/keboolaasia/python-dynamics365-client.git@0.3.2

COPY . /src/

CMD python3 -u /src/main.py
