VERSION=0.1.9
IMAGE=pocin/kbc-ex-dynamics365
TESTCOMMAND="docker run --rm -it --entrypoint '/bin/ash' -v `pwd`:/src/ -e KBC_DATADIR='/src/tests/data/' ${IMAGE}:latest /src/run_tests.sh"
test:
	eval $(TESTCOMMAND)
test-prod:
	docker run --rm -it --entrypoint '/bin/sh' -v `pwd`/tests/env.sh:/src/tests/env.sh ${IMAGE}:${VERSION} /src/run_tests.sh
run:
	docker run --rm -v `pwd`:/src/ -e KBC_DATADIR=/data/ ${IMAGE}:latest

prod:
	docker run --rm -it --entrypoint "/bin/ash" ${IMAGE}:latest
sh:
	docker run --rm -it --entrypoint "/bin/ash" -v `pwd`:/src/ -e KBC_DATADIR=/data/ ${IMAGE}:latest

build:
	echo "Building ${IMAGE}:${VERSION}"
	docker build -t ${IMAGE}:${VERSION} -t ${IMAGE}:latest .

deploy:
	echo "Pusing to dockerhub"
	docker push ${IMAGE}:${VERSION}
	docker push ${IMAGE}:latest
