"""
KBC extractor
"""
import logging
import os
import csv
import json
import pathlib
import re
from dynamics365 import DynamicsClient


REQUIRED_AUTH_FIELDS = ['client_id', '#client_secret', 'redirect_uri',
                        'resource', '#refresh_token']
REQUIRED_INCEREMNTAL_LOAD_FIELDS = ['primary_key', 'endpoint']
REQUIRED_LOAD_ALL_FIELDS = ['endpoint']
class ConfigError(ValueError):
    """Invalid extractor configuration"""


def _verify_load_by_modified_date_config(params, errors):
    try:
        cfg_incremental = params['extract_by_modifiedon']
    except KeyError:
        logging.info("No \"extract_by_modifiedon\" config")
    else:
        if 'hours_back' not in cfg_incremental:
            errors.append("extract_by_modifiedon.hours_back is missing!")
        for endpoint in cfg_incremental['endpoints']:

            for field in REQUIRED_INCEREMNTAL_LOAD_FIELDS:
                if field not in endpoint:
                    errors.append(
                        ("The 'extract_by_modifiedon' endpoint '{}' "
                         "must have defined the field '{}' ").format(endpoint,
                                                                     field))
    return errors

def _verify_extract_all_config(params, errors):
    try:
        cfg_all_rows = params['extract_all']
    except KeyError:
        logging.info("No \"extract_all\" config")
    else:
        for endpoint in cfg_all_rows:
            for field in REQUIRED_LOAD_ALL_FIELDS:
                if field not in endpoint:
                    errors.append(
                        ("The 'extract_all' endpoint '{}' must have "
                         "defined the field '{}'").format(endpoint, field))
    return errors

def _verify_extract_optionset_config(params, errors):
    try:
        cfg_all_rows = params['extract_option_set']
    except KeyError:
        logging.info("No \"extract_option_set\" config")
    else:
        for endpoint in cfg_all_rows:
            for field in ['attribute', 'entity', 'tablename']:
                if field not in endpoint:
                    errors.append(
                        ("The 'extract_option_set' endpoint '{}' must have "
                         "defined the field '{}'").format(endpoint, field))
    return errors
def verify_config(params):
    # check auth data is there

    errors = []
    try:
        auth = params['auth']
    except KeyError:
        errors.append('Provide "auth" data')
    else:
        for field in REQUIRED_AUTH_FIELDS:
            if field not in auth:
                errors.append("auth.{} is missing!".format(field))


    _verify_load_by_modified_date_config(params, errors)
    _verify_extract_all_config(params, errors)
    _verify_extract_optionset_config(params, errors)

    for error in errors:
        logging.error(error)
    if errors:
        raise ConfigError("Check above errors in config.json and fix them.")



def serialize_rows(data, outfile):
    """Consume the data generator and write it to the outfile"""

    logging.info("Serializing response into %s", outfile)
    try:
        first_row = next(data)
    except StopIteration:
        logging.info("No data in %s", outfile)
    else:
        with open(outfile, 'w') as outf:
            writer = csv.DictWriter(outf, fieldnames=first_row.keys())
            writer.writeheader()
            writer.writerow(first_row)
            writer.writerows(data)
    return outfile

def write_incremental_manifest(outfile, pkeys):
    """Indicate primary keys and incremental uplaod

    No need to change tablename
    """
    manifest = {
        "incremental": True,
        "primary_key": pkeys}
    manifest_path = outfile + '.manifest'
    with open(manifest_path, 'w') as outf:
        json.dump(manifest, outf)
    return manifest_path

def _construct_tablename(config_dict):
    """
    Args:
        config_dict: with fields 'endpoint' and optionally 'tablename'

    """
    try:
        tablename = pathlib.Path(config_dict['tablename']).stem + ".csv"
    except KeyError:
        # tablename is not defined, use endpoint to derive a name
        tablename = config_dict['endpoint'].split('?')[0] + ".csv"
    return tablename


def download_rows_by_modified_date(config, client, outdir):
    """
    The relevant sub-json must contain
    "hours_back": 72
    "endpoints": [

   ]
    "endpoint"
    "primary_key"
    optionally "tablename" for custom tablename. Otherwise it's constructed from "endpoint"


    i.e something like this:
        "extract_by_modifiedon": {
            "hours_back": "72",
            "endpoints": [
                {
                    "endpoint": "accounts?$select=acccountid,foo,bar",
                    "tablename": "custom_accounts_tablename",
                    "primary_key": ["accountid"]
                }
                {
                    "endpoint": "contacts?$select=contactid,foo,bar",
                    "primary_key": ["contactid"]
                }
            ]
        }

    for the contacts endpoint the tablename will be "contacts"
    """
    logging.info("Downloading tables by modified date")
    try:
        if config['hours_back'] == 'infinity':
            logging.info("Since 'hours_back' is set to 'infinity'"
                         "the whole history is downloaded")
            q_filter = None
        else:
            hours_back = int(config['hours_back'])
            _f_template = ("Microsoft.Dynamics.CRM.LastXHours"
                       "(PropertyName='modifiedon',PropertyValue='{hours}')")
            q_filter = {"$filter": _f_template.format(hours=hours_back)}
    except (KeyError, TypeError):
        raise ConfigError(
            "When downloading rows by modifiedon you must specify offset"
            " in the parameter 'hours_back' and it must be"
            "an integer or 'infinity'")

    # epc for EndPointConfig
    for epc in config["endpoints"]:
        logging.info("Downloading %s", epc)
        rows = client.get(epc['endpoint'], params=q_filter)
        tablename = _construct_tablename(epc)
        outfile = os.path.join(outdir, tablename)
        serialize_rows(data=rows, outfile=outfile)
        # the file is written only if some data was downloaded
        if os.path.isfile(outfile):
            write_incremental_manifest(outfile=outfile, pkeys=epc['primary_key'])


def download_all_rows(config, client, outdir):
    """
    Just download all rows for given endpoint

    Relevant config

        "extract_all": [
            {
                "endpoint": "accounts?$select=acccountid,foo,bar",
                "tablename": "custom_tablename_IF_omitted_use_the_root_of_endpoint"
            },
            {
                "endpoint": "contacts"
            }
        ]
    """
    logging.info("Downloading all rows for tables")
    for epc in config:
        logging.info("Processing %s", epc)
        rows = client.get(epc['endpoint'])
        tablename = _construct_tablename(epc)
        outfile = os.path.join(outdir, tablename)
        serialize_rows(data=rows, outfile=outfile)


def _download_and_serialize_option_set(client, entity_name, attribute_name):
    """Download all options for given entity attribute

    applicable when entity has a dropdown menu for picking

    Returns: a generator yielding a {'value': 85340001, 'label': 'Corporate'}
        for each available option


    """
    url_template = (
        "EntityDefinitions(LogicalName='{entity_name}')"
        "/Attributes(LogicalName='{attribute_name}')"
        "/Microsoft.Dynamics.CRM.PicklistAttributeMetadata"
        "?$select=LogicalName"
        "&$expand=OptionSet($select=Options),GlobalOptionSet($select=Options)"
    ).format(entity_name=entity_name,
             attribute_name=attribute_name)
    response = client.get_one(url_template)

    opts = response.get('OptionSet') or response.get('GlobalOptionSet')
    for opt in opts["Options"]:
        yield {
            'value': opt['Value'],
            'label': opt['Label']['UserLocalizedLabel']['Label']
        }


def download_option_set(client, config, outdir):
    logging.info("Downloading option_sets")

    for option in config:
        logging.info("Processing %s", option)
        rows = _download_and_serialize_option_set(
            client,
            option['entity'],
            option['attribute'])
        tablename = pathlib.Path(option['tablename']).stem + ".csv"
        outfile = os.path.join(outdir, tablename)
        serialize_rows(data=rows, outfile=outfile)

def main(params, outdir):
    """Run the extractor"""
    auth = params['auth']
    api = DynamicsClient(client_id=auth['client_id'],
                         client_secret=auth['#client_secret'],
                         refresh_token=auth['#refresh_token'],
                         resource=auth['resource'],
                         redirect_uri=auth['redirect_uri'])
    verify_config(params)

    with api:
        modified_by_cfg = params.get('extract_by_modifiedon')
        if modified_by_cfg is not None:
            download_rows_by_modified_date(config=modified_by_cfg, client=api, outdir=outdir)

        all_tables_cfg = params.get('extract_all')
        if all_tables_cfg is not None:
            download_all_rows(client=api, config=all_tables_cfg, outdir=outdir)

        optionset_cfg = params.get('extract_option_set')
        if optionset_cfg is not None:
            download_option_set(client=api, config=optionset_cfg, outdir=outdir)

