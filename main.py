#!/bin/env python3
import logging
import sys
import os
import traceback
from keboola import docker
from exdynamics import main
from exdynamics.extractor import verify_config

if __name__ == "__main__":
    try:
        datadir = os.getenv('KBC_DATADIR')

        cfg = docker.Config(data_dir=datadir)
        params = cfg.get_parameters()

        if params.get('debug'):
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.INFO)


        outdir = os.path.join(datadir, 'out/tables')
        try:
            os.makedirs(outdir)
        except FileExistsError:
            logging.debug(outdir + " already exists")

        main(params, outdir)
    except (ValueError, KeyError) as err:
        traceback.print_exc(file=sys.stderr)
        print(err, file=sys.stderr)
        sys.exit(1)
    except Exception as err:
        traceback.print_exc(file=sys.stderr)
        print(err, file=sys.stderr)
        sys.exit(2)
        raise
