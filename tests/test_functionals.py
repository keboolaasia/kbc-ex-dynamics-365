import os
import pytest
import json
import csv

from os.path import join as pjoin
from exdynamics import main
from exdynamics.extractor import verify_config, ConfigError, _construct_tablename
import logging


@pytest.fixture
def common_config():
    return {
        "debug": True,
        "auth": {
            "client_id": os.getenv("EX_CLIENT_ID"),
            "#client_secret": os.getenv("EX_CLIENT_SECRET"),
            "redirect_uri": os.getenv("EX_REDIRECT_URI"),
            "resource": os.getenv("EX_RESOURCE"),
            "#refresh_token": os.getenv("EX_REFRESH_TOKEN")
        }
    }


def test_constructing_tablenames():
    assert (_construct_tablename({'tablename':'accounts_last_7200_hours'})
            == 'accounts_last_7200_hours.csv')

    assert (_construct_tablename({'tablename':'accounts_last_7200_hours.csv'})
            == 'accounts_last_7200_hours.csv')

    assert _construct_tablename({'endpoint':'accounts'}) == 'accounts.csv'

    assert _construct_tablename({'endpoint':'accounts?foo=bar'}) == 'accounts.csv'

def test_verifying_incremental_config_ok(common_config):
    common_config['extract_by_modifiedon'] = {
        "hours_back": 72,
        "endpoints": [
            {'endpoint': 'foo',
             'primary_key': ['bar']}
        ]
    }
    verify_config(common_config)

def test_verifying_all_rows_config_ok(common_config):
    cfg = common_config
    cfg["extract_all"] = [
            {
                "endpoint": "accounts?$top=300",
            },
            {
                "endpoint": "contacts?$top=300",
                "tablename": "contacts_top300"
            }
        ]

def test_verifying_malformed_config_fails(common_config):
    err_config = common_config
    del err_config['auth']['client_id']

    with pytest.raises(ConfigError) as out:
        verify_config(err_config)

def test_downloading_tables_by_modifiedon(tmpdir, common_config):
    outdir = tmpdir.mkdir('outfoo').strpath

    outtablename = "accounts_last_7200_hours"
    common_config["extract_by_modifiedon"] = {
        "hours_back": "7200",
        "endpoints": [
            {
                "endpoint": "accounts?$top=10",
                "tablename": outtablename,
                "primary_key": ["accountid"]
            }
        ]
    }
    main(params=common_config, outdir=outdir)

    path_table1 = pjoin(outdir, outtablename + '.csv')

    assert os.path.isfile(path_table1)
    with open(path_table1) as fin:
        r = csv.DictReader(fin)
        row = next(r)
        assert 'accountid' in row

    path_manifest = pjoin(outdir, outtablename + '.csv.manifest')
    assert os.path.isfile(path_manifest)

    with open(path_manifest, 'r') as fin:
        mani = json.load(fin)
        assert mani['primary_key'] == ['accountid']
        assert mani['incremental']

def test_downloading_all_tables(tmpdir, common_config):
    outdir = tmpdir.mkdir('out').strpath

    cfg = common_config

    cfg["extract_all"] = [
        {
            "endpoint": "accounts",
        },
        {
            "endpoint": "contacts?$top=300",
            "tablename": "contacts_top300"
        }
    ]
    main(params=cfg, outdir=outdir)

    accounts = pjoin(outdir, 'accounts.csv')
    assert os.path.isfile(accounts)
    with open(accounts) as fin:
        r = csv.DictReader(fin)
        row = next(r)
        assert 'accountid' in row
    assert os.path.isfile(pjoin(outdir, 'contacts_top300.csv'))

def test_downloading_option_set(tmpdir, common_config):
    outdir = tmpdir.mkdir('out').strpath
    cfg = common_config
    attribute = 'esp_individualorcorporate'
    tablename = attribute + '_optionset'
    cfg['extract_option_set'] = [
        {
            'entity': 'esp_grouping',
            'attribute': attribute,
            'tablename': tablename
        }
    ]
    main(params=cfg, outdir=outdir)

    outpath = pjoin(outdir, tablename + '.csv')
    with open(outpath) as fin:
        reader = csv.DictReader(fin)
        line = next(reader)
        assert set(line.keys()) == set(['value', 'label'])
